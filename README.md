# Popis
Projekt bol vypracovany v Jupyter notebook s využitím knižníc spectrum, scipy, matploitlib, numpy a warnings

# Inštalácia Jupyter-u

```
#!bash

pip3 install jupyter
```

# Spustenie projektu

```
#!bash

jupyter notebook
```
Kernel -> Restart & Run All

# Zadanie
Zadanie je k dispozicii v subore: zadanie.pdf